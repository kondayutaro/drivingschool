from datetime import datetime
from bs4 import BeautifulSoup
from config import Config
from parse_regex import ParseRegex
from web_driver import WebDriver


def main():
    is_in_production = Config.isInProduction

    # login
    driver = WebDriver()
    print('driver launched: ' + str(datetime.now().time()))
    source = driver.login() if is_in_production \
        else open('豊島自動車練習所-インターネット予約システム.html')

    if source:
        # When the page is not in maintenance
        soup = BeautifulSoup(source, 'lxml')
        days = soup.find_all(class_='date')
        parse_body = ParseRegex()
        parse_body.get_dates_and_availability(days)

        print('driver closed: ' + str(datetime.now().time()))
        driver.log_out()


if __name__ == '__main__':
    main()

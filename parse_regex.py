import re
import rx
from rx import operators as ops
from bs4 import ResultSet
import time_date_format
from telegram_bot import TelegramBot


class ParseRegex:

    def __init__(self):
        self.time_slot_regex = re.compile(r"sendContent\('\d\d\d\d(\d\d\d\d)','(\d)'")
        self.date_regex = re.compile(r'\((\w)\)')
        self.month_date_regex = re.compile(r'(\d\d)\w(\d\d)')
        self.text = ''
        self.bot = TelegramBot()

    def get_dates_and_availability(self, days: list) -> ():
        rx.from_(days).pipe(
            # Select days with available slots
            # Empty slots are included
            ops.filter(lambda day: day.select('.status1 a')),
            # Limit to weekends
            ops.filter(lambda day: day.select('.view font')),
            # Get date
            ops.do_action(
                on_next=lambda day: self.get_day(day.select('.view font'))
            ),
            # Get rid of empty slots
            ops.map(lambda day: day.select('.status1 a'))
        ).subscribe(
            # Get available slot information
            on_next=lambda slots: self.get_slots(slots),
            # Post text
            on_completed=lambda: self.post_only_unempty_text(self.text)
        ).dispose()

    def post_only_unempty_text(self, text):
        if text:
            self.bot.post_production(text)

    def get_slots(self, slots: list):
        # For getting slots
        post_text = ''
        for slot in slots:
            regex = self.time_slot_regex.search(slot['onclick'])
            slot = time_date_format.time_converter(regex.group(2))
            post_text += slot + '\n'
        self.text += post_text

    def get_day(self, days: ResultSet):
        # For getting date
        post_text = ''
        for d in days:
            post_text += '\n'
            month_date = self.month_date_regex.search(d.contents[0].strip())
            month = month_date.group(1)
            day = month_date.group(2)
            post_text += time_date_format.day_converter(day) + '\t' + time_date_format.month_converter(month) + '\t'
            post_text += time_date_format.date_converter(self.date_regex.search(d.contents[2].strip()).group(1))
            post_text += '\n'
        self.text += post_text

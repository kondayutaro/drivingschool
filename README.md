# 豊島自動車学校空席通知

## 概要
* herokuの巡回設定に応じて定期的に豊島自動車学校の空きコマを通知する

## 使い方
* herokuでホストする
* ローカルの場合は`pipenv install`
* Telegramのbotチャンネルに登録する
from dataclasses import dataclass

@dataclass
class Config:
    # Supply driving shool's login url
    loginUrl: str = ""
    userName: str = ""
    password: str = ""
    # Access to the actual website if true
    isInProduction: bool = False
